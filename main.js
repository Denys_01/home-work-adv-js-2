const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];

const ulElement = document.createElement("ul");
const divElement = document.querySelector("#root");


books.forEach((book) => {
    try {
        if (!book.author  || !book.name || !book.price) {
            throw new Error("Некоректнная структура");
        }
    const liElement = document.createElement("li");
    liElement.textContent = book.author + " " + book.name + " " + book.price;
    ulElement.append(liElement);
    } catch (exception) {
    console.error(exception.message);
    }finally{// это  творчество от себя))
       const pElement = document.createElement('p');
       pElement.textContent = " :{)  "
       divElement.append(pElement);
    }

});
divElement.append(ulElement);